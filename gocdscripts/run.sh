#!/bin/bash
source ./gocdscripts/variable.sh
export KUBECONFIG=/admin.conf

kubectl run ${deployed_pod_name,,} --image=${domain_name}/${server_username}/${go_pipeline_name,,}:$GO_PIPELINE_LABEL --as=superman --as-group=system:masters
