#!/bin/bash

source ./gocdscripts/variable.sh

docker build \
    --build-arg NODE_ENV=$NODE_ENV \
    -t ${domain_name}/${server_username}/${go_pipeline_name,,}:$GO_PIPELINE_LABEL .

